<?php

return [
    'channel_id' => env('MESSAGEBIRD_CHANNEL'),
    'namespace' => env('MESSAGEBIRD_NAMESPACE'),
    'api_key' => env('MESSAGEBIRD_KEY'),
];
