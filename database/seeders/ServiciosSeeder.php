<?php

namespace Database\Seeders;

use App\Models\Servicio;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ServiciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $array = [
            'gogosender.com',
            'app.disruptive.center',
            'app.gosen.io',
            'gogoclick.info',
            'adriantrigo.com'
        ];

        foreach ($array as $dominio) {
            Servicio::updateOrCreate([
                'dominio' => $dominio
            ], [
                'dominio' => $dominio
            ]);
        }
    }
}
