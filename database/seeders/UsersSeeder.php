<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (config('app.env')=='production') {
            $array = [
                ['Mario','maritojhefi@gmail.com','+59175140175'],
                ['Alejandro','echeko11@gmail.com','+59179260429'],
                ['Rodrigo','villaortiz110@gmail.com','+59175141260'],
                ['Adrian','adrian.trigo@gmail.com','+59175031955'],
                ['Walter','walterrt97@gmail.com','+59170212752'],
            ];
    
            foreach ($array as $data) {
                User::updateOrCreate([
                    'email' => $data[1]
                ], [
                    'email' => $data[1],
                    'telf' => $data[2],
                    'name' => $data[0],
                    'password' => Hash::make(12345678)
                ]);
            }
        }
       
    }
}
