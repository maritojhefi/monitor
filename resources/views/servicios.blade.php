<!DOCTYPE html>
<html lang="en" style="height: 100% !important;">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
        integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        @import url(http://weloveiconfonts.com/api/?family=entypo);


        [class*="entypo-"]:before {
            font-family: 'entypo', sans-serif;
        }

        html {
            background: #f1f1ee;

            color: #333;
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 10px;
            -webkit-font-smoothing: antialiased;
            font-style,
            font-weight,
            letter-spacing,
            line-height,
            word-spacing: normal;
        }

        b {
            font-weight: bold;
        }

        header {
            margin: 0 auto;
            width: 700px;
        }

        .wrapper {
            margin: 0 auto 30px;
            width: 700px;
            box-shadow: 0 0 5px #ddd;
        }

        .logo {
            margin: 30px 0;
            width: 69px;
            height: 29px;
            background: url(https://goo.gl/utqKP);
            display: block;
        }

        section {
            background: #fff;
        }

        .status {
            border-radius: 4px 4px 0 0;
            border-bottom: 1px solid #eee;
        }

        .status article {
            padding: 50px 30px;
        }

        .status .led {
            width: 15px;
            height: 15px;
            border-radius: 100%;
            display: inline-block;
        }

        .status h2 {
            margin: 0 21px;
            display: inline-block;
            font-size: 2.2rem;
        }

        .status p {
            margin: 10px 39px 0;

            color: #999;
            font-size: 1.3rem;
        }

        .info {
            border-bottom: 1px solid #eee;
        }

        .info article {
            padding: 15px 0 30px 69px;
        }

        .info .service-label {
            float: left;

            color: #999;
            font-size: 1.3rem;
        }

        .info .duration-label {
            float: left;
            margin: 0 0 0 55px;

            color: #999;
            font-size: 1.3rem;
        }

        .stats article {
            padding: 60px 70px 10px 70px;
        }

        .stats article:last-child {
            padding: 60px 70px 75px 70px;
        }

        .service {
            float: left;
            font-size: 1.3rem;
            font-weight: bold;
        }

        .chart {
            float: right;
        }

        .bar {
            float: left;
            width: 4px;
            height: 15px;
            border-left: 1px solid #fff;
            cursor: pointer;
        }

        .legend {
            border-top: 1px solid #eee;
            border-radius: 0 0 4px 4px;
        }

        .legend article {
            padding: 30px 0 40px 197px;
        }

        .legend-colour {
            margin: 0 0 0 20px;
            float: left;
            width: 10px;
            height: 10px;
        }

        .legend p {
            float: left;
            margin: 0 10px;

            color: #777;
            font-size: 1.1rem;
        }

        .green {
            background: #17d766;
            background: -moz-linear-gradient(top, #17d766 0%, #17d766 50%, #16cf62 51%, #16cf62 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #17d766), color-stop(50%, #17d766), color-stop(51%, #16cf62), color-stop(100%, #16cf62));
            background: -webkit-linear-gradient(top, #17d766 0%, #17d766 50%, #16cf62 51%, #16cf62 100%);
            background: -ms-linear-gradient(top, #17d766 0%, #17d766 50%, #16cf62 51%, #16cf62 100%);
            background: linear-gradient(to bottom, #17d766 0%, #17d766 50%, #16cf62 51%, #16cf62 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#17d766', endColorsGradientType=0);
            opacity: 0.8;
            filter: alpha(opacity=80);
        }

        .green-text {
            color: #17d766;
        }

        .yellow {
            background: #ffcc00;
            background: -moz-linear-gradient(top, #ffcc00 0%, #ffcc00 50%, #eabb00 51%, #eabb00 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffcc00), color-stop(50%, #ffcc00), color-stop(51%, #eabb00), color-stop(100%, #eabb00));
            background: -webkit-linear-gradient(top, #ffcc00 0%, #ffcc00 50%, #eabb00 51%, #eabb00 100%);
            background: -ms-linear-gradient(top, #ffcc00 0%, #ffcc00 50%, #eabb00 51%, #eabb00 100%);
            background: linear-gradient(to bottom, #ffcc00 0%, #ffcc00 50%, #eabb00 51%, #eabb00 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffcc00', endColorstr='#eabb00', GradientType=0);
            opacity: 0.8;
            filter: alpha(opacity=80);
        }

        .yellow-text {
            color: #ffcc00;
        }

        .red {
            background: #f23311;
            background: -moz-linear-gradient(top, #f23311 0%, #f23311 50%, #e32c0c 51%, #e32c0c 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #f23311), color-stop(50%, #f23311), color-stop(51%, #e32c0c), color-stop(100%, #e32c0c));
            background: -webkit-linear-gradient(top, #f23311 0%, #f23311 50%, #e32c0c 51%, #e32c0c 100%);
            background: -ms-linear-gradient(top, #f23311 0%, #f23311 50%, #e32c0c 51%, #e32c0c 100%);
            background: linear-gradient(to bottom, #f23311 0%, #f23311 50%, #e32c0c 51%, #e32c0c 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f23311', endColorstr='#e32c0c', GradientType=0);
            opacity: 0.8;
            filter: alpha(opacity=80);
        }

        .yellow-red {
            color: #f23311;
        }

        .tipsy {
            position: absolute;
            z-index: 999;
            padding: 5px;

            font-size: 1.1rem;
        }

        .tipsy-inner {
            padding: 7px 12px 8px;
            max-width: 200px;
            background: #000;
            border-radius: 4px;
            box-shadow: 0 1px 5px #777;

            color: #FFF;
            line-height: 1.4rem;
            text-align: center;

        }

        .tipsy-arrow {
            position: absolute;
            width: 0;
            height: 0;
            border: 5px dashed #000;

            line-height: 0;
        }

        .tipsy-arrow-s {
            border-top-color: #000;
        }

        .tipsy-s .tipsy-arrow {
            margin-left: -5px;
            bottom: 0;
            left: 50%;
            border-top-style: solid;
            border-right-color: transparent;
            border-bottom: none;
            border-left-color: transparent;
        }

        .twitter {
            border-bottom: 1px solid #eee;
        }

        .twitter article {
            padding: 30px 30px 43px;
        }

        .entypo-twitter {
            float: left;
            margin: -3px 0 0;

            color: #11bff2;
            font-size: 1.7rem;
        }

        .twitter a {
            float: left;
            margin: 0 26px;
            padding: 0 0 2px;
            border-bottom: 1px dotted #ccc;

            color: #999;
            font-size: 1.3rem;
            text-decoration: none;

            -webkit-transition: 0.5s;
            -moz-transition: 0.5s;
            transition: 0.5s;
        }

        .twitter a:hover {
            color: #333;
            border-bottom: 1px dotted #333;
        }

        .tweet {
            border-radius: 0 0 4px 4px;
        }

        .tweet article {
            padding: 30px 65px 30px 30px;

            color: #999;
            font-size: 1.3rem;
            line-height: 2.3rem;
        }

        .tweet-date {
            float: left;
            margin: 0 0 0 44px;

            color: #ccc;
            font-weight: bold;
        }

        .tweet p {
            margin: 0 0 30px 188px;
        }

        footer {
            margin: 0 auto 100px;
            width: 700px;
        }

        footer nav {
            padding: 20px 30px;
            background: #fff;
            border-radius: 4px;
            box-shadow: 0 0 5px #ddd;
        }

        footer li {
            margin: 10px;
            display: inline-block;
        }

        footer li:last-child {
            float: right;
        }

        footer a {
            color: #999;
            font-size: 1.3rem;
            text-decoration: none;

            -webkit-transition: 0.5s;
            -moz-transition: 0.5s;
            transition: 0.5s;
        }

        footer a:hover {
            color: #333;
        }
    </style>
</head>

<body style="width: 100%;height: 100% !important;">

    {{-- <div class="accordion" id="accordionExample" style="width: 100%">
                @foreach ($servicios as $servicio)
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapse{{ $servicio->id }}" aria-expanded="true"
                                aria-controls="collapse{{ $servicio->id }}">
                                {{ $servicio->dominio }} - codigo de estado ({{ $servicio->detalle->estado }}) <i
                                    class="ms-2 fa fa-circle"
                                    style="color: {{ $servicio->detalle->estado == 200 ? 'green' : 'red' }}"></i>
                            </button>
                        </h2>
                        <div id="collapse{{ $servicio->id }}" class="accordion-collapse collapse show"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <div class="card-head has-aside">
                                    <h4 class="card-title card-title-sm"></h4>
                                    <div class="card-opt">
                                        <a href="{{ url()->current() }}?user=1">1 Day</a>
                                        <a href="{{ url()->current() }}?user=7">7 Days</a>
                                        <a href="{{ url()->current() }}?user=15">15 Days</a>
                                        <a href="{{ url()->current() }}?user=30">30 Days</a>
                                        <a href="{{ url()->current() }}?user=365">365 Days</a>
                                    </div>
                                </div>
                                <div class="chart-statistics mb-0" style="height: 480px">
                                    <canvas id="allDataChart{!! $servicio->id !!}"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div> --}}
    <div class="container">
        {{-- <div class="card text-left">
            <img class="card-img-top" src="holder.js/100px180/" alt="">
            <div class="card-body">
                <div class="row">
                    <div class="col-4">
                        <div class="list-group" id="list-tab" role="tablist">
                            @foreach ($servicios as $servicio)
                                <a class="list-group-item list-group-item-action {{ $loop->iteration == 1 ? 'active' : '' }}"
                                    id="list-home-list" data-bs-toggle="list" href="#list-{{ $loop->iteration }}"
                                    role="tab" aria-controls="list-home">{{ $servicio->dominio }} - codigo de estado
                                    ({{ $servicio->detalle->estado }})
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="tab-content" id="nav-tabContent">
                            @foreach ($servicios as $servicio)
                                <div class="tab-pane fade {{ $loop->iteration == 1 ? 'show active' : '' }}"
                                    id="list-{{ $loop->iteration }}" role="tabpanel" aria-labelledby="list-home-list">
                                    <canvas id="allDataChart{!! $servicio->id !!}"></canvas>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}





        <div class="wrapper" style="width: 100% !important">
            <section class="status">
                <article>
                    <i class="fa fa-signal" style="color: green; font-size: 33px;"></i>
                    <h2>Estado actual de las páginas.</h2>
                    <p class="ms-1">Hoy es {{ now()->format('y-m-d H:i A') }}</p>
                </article>
            </section>

            <section class="info">
                <article>
                    <div class="service-label me-5">Dominios</div>
                    <div class="duration-label" style="margin-left: 25%;">Grafica del estado actual de acuerdo a las
                        incidencias (registros)
                    </div>
                </article>
            </section>
            <hr>



            <div class="row mx-4">
                <div class="col-4">
                    <div class="list-group" id="list-tab" role="tablist">
                        @foreach ($servicios as $servicio)
                            <a class="p-5 service list-group-item list-group-item-action {{ $loop->iteration == 1 ? 'active' : '' }}"
                                id="list-home-list" data-bs-toggle="list" href="#list-{{ $loop->iteration }}"
                                role="tab" aria-controls="list-home"><i
                                    class="fa-xl fa fa-{{ $servicio->estado == 200 ? 'signal' : 'triangle-exclamation' }} me-4"
                                    style='color : {{ $servicio->estado == 200 ? '#2ac02a' : 'red' }}'></i>{{ $servicio->dominio }}
                                ({{ $servicio->estado }})
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="col-8">
                    <div class="tab-content" id="nav-tabContent">
                        @foreach ($servicios as $servicio)
                            {{-- <div class="card-head has-aside">
                                <h4 class="card-title card-title-sm"></h4>
                                <div class="card-opt">
                                    <a href="{{ url()->current() }}?user=1">1 Day</a>
                                    <a href="{{ url()->current() }}?user=7">7 Days</a>
                                    <a href="{{ url()->current() }}?user=15">15 Days</a>
                                    <a href="{{ url()->current() }}?user=30">30 Days</a>
                                    <a href="{{ url()->current() }}?user=365">365 Days</a>
                                </div>
                            </div> --}}
                            <div class="tab-pane fade {{ $loop->iteration == 1 ? 'show active' : '' }}"
                                id="list-{{ $loop->iteration }}" role="tabpanel" aria-labelledby="list-home-list">
                                <canvas id="allDataChart{!! $servicio->id !!}" style="height: 391px;"></canvas>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <section class="legend">
                <article>
                    <div class="legend-colour green"></div>
                    <p>Code 200 (OK)</p>

                    <div class="legend-colour red"></div>
                    <p>Code > 200 or < 200 (Error)</p>
                </article>
            </section>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"
        integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.js"
        integrity="sha512-+k1pnlgt4F1H8L7t3z95o3/KO+o78INEcXTbnoJQ/F2VqDVhWoaiVml/OEHv9HsVgxUaVW+IbiZPUJQfF/YxZw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/all.min.js"
        integrity="sha512-uKQ39gEGiyUJl4AI6L+ekBdGKpGw4xJ55+xyJG7YFlJokPNYegn9KwQ3P8A7aFQAUtUsAQHep+d/lrGqrbPIDQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        function createChart(ctx, labels, data, days) {
            new Chart(ctx, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [{
                        data: data,
                        label: 'Code',
                        borderColor: '#D1D1D1',
                        backgroundColor: function(context) {
                            var index = context.dataIndex;
                            var value = context.dataset.data[index];
                            return value === 200 ? 'green' : 'red';
                        },
                        pointStyle: 'circle',
                        pointRadius: 6,
                        pointHoverRadius: 10
                    }]
                },
                options: {
                    plugins: {
                        legend: {
                            display: false
                        },
                    },
                    scales: {
                        y: {
                            beginAtZero: false,
                            reverse: true // Invertir el eje Y
                        },
                        x: {
                            min: 0,
                            max: 500,
                            type: days === 1 ? 'time' : 'category',
                            time: {
                                unit: 'hour',
                                displayFormats: {
                                    hour: 'D MMM H:mm'
                                }
                            }
                        },
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                }
            });
        }
    </script>
    <script>
        $('.bar').tipsy({
            gravity: 's',
            html: true,
            offset: 2
        });
    </script>
    @foreach ($servicios as $servicio)
        <script>
            var ctx{!! $servicio->id !!} = document.getElementById('allDataChart{!! $servicio->id !!}');
            var dataArray{!! $servicio->id !!} = [{!! $chartData[$servicio->id]['data'] !!}];
            var labelsArray{!! $servicio->id !!} = [{!! $chartData[$servicio->id]['labels'] !!}];
            createChart(ctx{!! $servicio->id !!}, labelsArray{!! $servicio->id !!}, dataArray{!! $servicio->id !!});
        </script>
    @endforeach
</body>

</html>
