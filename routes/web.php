<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ServiciosController;
use App\Models\Servicio;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [ServiciosController::class, 'index']);
// Route::get('/servicios', [ServiciosController::class, 'index']);

Route::get(
    '/prueba/email',
    function () {
        $servicio = Servicio::find(1);
        $nameServer = '101 digital Server';
        return view('mail.mailChangeStatusService', compact('servicio', 'nameServer'));
    }
);
Route::get('/prueba/ip', function () {
    $ch = curl_init('http://ipinfo.io/ip');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $publicIp = curl_exec($ch);
    curl_close($ch);
    dd($publicIp);
});
