<?php

namespace App\Models;

use App\Models\DetalleServicio;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Servicio extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'dominio',
        'estado',
        'password',
    ];

    public function detalle()
    {
        return $this->hasOne(DetalleServicio::class, 'id_servicio', 'id');
    }
}
