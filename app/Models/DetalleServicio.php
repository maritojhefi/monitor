<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleServicio extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_servicio',
        'estado',
        'actualizacion_estado',
        'descripcion'
    ];
}
