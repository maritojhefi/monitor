<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Servicio;
use App\Models\DetalleServicio;
use Illuminate\Console\Command;
use GuzzleHttp\Psr7\ServerRequest;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use App\Helpers\WhatsappNotification;
use App\Mail\ChangeStatusServicesMail;

class StatusPagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'status:pages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revisa los estados de los dominios registrados en DB y envia notificaciones por email y whatsapp si ocurre un error';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $servicios = Servicio::all();
        $users = User::all();
        $bar = $this->output->createProgressBar(count($servicios));
        $bar->start();
        $nameServer = 'Desconocido';
        try {
            $ch = curl_init('http://ipinfo.io/ip');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $publicIp = curl_exec($ch);
            curl_close($ch);
            switch ($publicIp) {
                case '204.48.26.172':
                    $nameServer = 'Gosen Server';
                    break;
                case '147.182.189.73':
                    $nameServer = '101 digital Server';
                    break;
                default:
                    # code...
                    break;
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
        foreach ($servicios as $servicio) {
            try {
                $respuesta = Http::get('https://' . $servicio->dominio);
                $this->line(' respuesta para dominio : ' . $servicio->dominio . ', estado : ' . $respuesta->status());
                $detalleServicio = DetalleServicio::where('id_servicio', $servicio->id)->orderBy('id', 'DESC')->first();
                if ($detalleServicio) {
                    if ($detalleServicio->estado != $respuesta->status()) {
                        DetalleServicio::create([
                            'id_servicio' => $servicio->id,
                            'estado' => $respuesta->status(),
                            'actualizacion_estado' => Carbon::now()
                        ]);
                        $servicio->estado = $respuesta->status();
                        $servicio->save();
                        if (!$respuesta->successful()) { //respuesta incorrecta y procede a enviar notificaciones a administradores
                            $this->info('Notificando por cambio de estado a error');
                            foreach ($users as $usuario) {
                                WhatsappNotification::startTemplate()
                                    ->templateName('_notificacion_dominio_error')
                                    ->to($usuario->telf)
                                    ->body([
                                        $servicio->dominio,
                                        $respuesta->status(),
                                        $nameServer
                                    ])
                                    ->lang('es')
                                    ->send();
                                Mail::to($usuario->email)->send(new ChangeStatusServicesMail($servicio, $nameServer));
                            }
                        } else {
                            $this->info('Notificando por cambio de estado a ok');
                            foreach ($users as $usuario) {
                                WhatsappNotification::startTemplate()
                                    ->templateName('_notificacion_dominio_ok')
                                    ->to($usuario->telf)
                                    ->body([
                                        $servicio->dominio,
                                        $nameServer
                                    ])
                                    ->lang('es')
                                    ->send();
                                Mail::to($usuario->email)->send(new ChangeStatusServicesMail($servicio, $nameServer));
                            }
                        }
                    }
                } else {
                    DetalleServicio::create([
                        'id_servicio' => $servicio->id,
                        'estado' => $respuesta->status(),
                        'actualizacion_estado' => Carbon::now()
                    ]);
                    $servicio->estado = $respuesta->status();
                    $servicio->save();
                }
                $bar->advance();
            } catch (\Throwable $th) {
                $detalleServicio = DetalleServicio::where('id_servicio', $servicio->id)->orderBy('id', 'DESC')->first();
                if ($detalleServicio) {
                    if ($detalleServicio->estado != '600' && $detalleServicio->wasChanged('estado')) {
                        $this->info('Enviando mensajes a administradores');
                        foreach ($users as $usuario) {
                            WhatsappNotification::startTemplate()
                                ->templateName('_notificacion_dominio_error')
                                ->to($usuario->telf)
                                ->body([
                                    $servicio->dominio,
                                    '600 : ' . $th->getMessage(),
                                    $nameServer
                                ])
                                ->lang('es')
                                ->send();
                            Mail::to($usuario->email)->send(new ChangeStatusServicesMail($servicio, $nameServer));
                        }
                    }
                } else {
                    DetalleServicio::create([
                        'id_servicio' => $servicio->id,
                        'estado' => '600',
                        'actualizacion_estado' => Carbon::now()
                    ]);
                }
            }
        }
        $bar->finish();
    }
}
