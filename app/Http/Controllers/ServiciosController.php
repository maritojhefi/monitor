<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Servicio;
use Illuminate\Http\Request;
use App\Models\DetalleServicio;

class ServiciosController extends Controller
{

    public function index(Request $request)
    {
        $servicios = Servicio::all();

        $days = $request->input('user', 15);
        $get_user = $days - 1;

        $chartData = [];

        foreach ($servicios as $servicio) {
            $chartData[$servicio->id] = $this->generateChartData($days, $servicio->id, $get_user);
            // Modificamos la consulta para obtener todos los detalles de servicio
            $chartData[$servicio->id]['allData'] = $this->getAllChartData($servicio->id);
        }

        return view('servicios', compact('servicios', 'chartData', 'days'));
    }
    private function getAllChartData($id_servicio)
    {
        $detalles = DetalleServicio::where('id_servicio', $id_servicio)
            ->orderBy('created_at', 'asc')
            ->get();

        $data = $detalles->pluck('estado')->implode(',');

        // Generar las etiquetas en el formato adecuado
        $labels = $detalles->pluck('created_at')->map(function ($item) {
            return $item->format('d M');
        })->toArray();

        return [
            'data' => $data,
            'labels' => '"' . implode('","', $labels) . '"', // Convertir las etiquetas a formato correcto
        ];
    }
    private function generateChartData($days, $id_servicio, $get_user)
    {
        $data = [];
        $labels = [];

        $detalles = DetalleServicio::where('id_servicio', $id_servicio)
            ->where('created_at', '>=', now()->subDays($days))
            ->orderBy('created_at', 'asc')
            ->get();

        foreach ($detalles as $detalle) {
            // Si es para un día, mostramos la fecha y hora
            if ($days === 1) {
                $labels[] = $detalle->created_at->format('j M | H:i A');
            } else {
                // Si es para más de un día, mostramos solo la fecha
                $labels[] = $detalle->created_at->format('j M | H:i A');
            }
            $data[] = $detalle->estado;
        }

        return [
            'data' => implode(',', $data),
            'labels' => '"' . implode('","', $labels) . '"',
        ];
    }
}
